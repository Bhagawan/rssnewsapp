package com.example.newscollector.util

import com.example.newscollector.data.News
import com.example.newscollector.data.SplashResponse
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.http.*

interface ServerClient {

    @FormUrlEncoded
    @POST("RssNewsApp/splash.php") @Json
    fun getSplash(@Field("locale") locale: String): Observable<SplashResponse>

    @GET @Xml
    fun getNews(@Url path : String): Observable<News>

    companion object {
        fun create() : ServerClient {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.newThread()))
                .addConverterFactory(XmlOrJsonConverterFactory())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(ServerClient::class.java)
        }
    }

}