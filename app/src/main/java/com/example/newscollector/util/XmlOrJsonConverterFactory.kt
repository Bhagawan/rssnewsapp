package com.example.newscollector.util

import com.google.gson.GsonBuilder
import com.tickaroo.tikxml.TikXml
import com.tickaroo.tikxml.retrofit.TikXmlConverterFactory
import okhttp3.ResponseBody
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Type


internal class XmlOrJsonConverterFactory : Converter.Factory() {

    override fun responseBodyConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<ResponseBody, *>? {
        for (annotation in annotations) {
            if (annotation.annotationClass == Xml::class)  return TikXmlConverterFactory.create(TikXml.Builder().exceptionOnUnreadXml(false).build())
                .responseBodyConverter(type, annotations, retrofit)
            else if (annotation.annotationClass == Json::class)  return GsonConverterFactory.create(
                GsonBuilder().create()
            ).responseBodyConverter(type, annotations, retrofit)
        }
        return super.responseBodyConverter(type, annotations, retrofit)
    }

}