package com.example.newscollector

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.newscollector.fragments.mvp.SplashPresenterViewInterface
import com.example.newscollector.fragments.mvp.SplashPresenter
import com.example.newscollector.util.MyWebChromeClient
import moxy.MvpAppCompatActivity
import moxy.presenter.InjectPresenter

@SuppressLint("CustomSplashScreen")
class SplashActivity : MvpAppCompatActivity(), SplashPresenterViewInterface {
    private lateinit var mWebView: WebView

    @InjectPresenter
    lateinit var mPresenter: SplashPresenter

    override fun onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack()
            return
        } else finish()
        super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        fullscreen()
        mWebView = findViewById(R.id.webView_splash)
        initialiseWebView()
        if (savedInstanceState == null) mPresenter.requestSplash()
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(event)
    }

    override fun switchToMain() {
        val i = Intent(applicationContext, MainActivity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NO_ANIMATION
        startActivity(i)
        finish()
    }

    override fun showError(error: String?) = Toast.makeText(this, error, Toast.LENGTH_SHORT).show()

    override fun showSplash(url: String) {
        mWebView = findViewById(R.id.webView_splash)
        mWebView.loadUrl(url)
    }

    override fun showLogo(logo: Bitmap?) {
        mWebView = findViewById(R.id.webView_splash)
        val logoLayout : ConstraintLayout = findViewById(R.id.layout_splash_logo)
        val imageLogo : ImageView = findViewById(R.id.imageView_splash_logo)
        imageLogo.setImageBitmap(logo)
        mWebView.visibility = View.GONE
        logoLayout.visibility = View.VISIBLE
    }

    override fun hideLogo() {
        mWebView = findViewById(R.id.webView_splash)
        val logoLayout = findViewById<ConstraintLayout>(R.id.layout_splash_logo)
        mWebView.visibility = View.VISIBLE
        logoLayout.visibility = View.GONE
    }


    @SuppressLint("SetJavaScriptEnabled")
    private fun initialiseWebView() {
        val frame = findViewById<FrameLayout>(R.id.webView_customView)
        mWebView.webViewClient = WebViewClient()
        mWebView.webChromeClient = MyWebChromeClient(this, mWebView, frame)
        val webSettings = mWebView.settings
        mWebView.scrollBarStyle = View.SCROLLBARS_OUTSIDE_OVERLAY
        val cookie = CookieManager.getInstance()
        cookie.setAcceptCookie(true)
        mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.allowContentAccess = true
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.allowUniversalAccessFromFileURLs = true
        webSettings.javaScriptEnabled = true
        webSettings.loadWithOverviewMode = true
        webSettings.allowFileAccess = true
        webSettings.domStorageEnabled = true
        webSettings.databaseEnabled = true
        webSettings.useWideViewPort = true
        webSettings.supportZoom()
        webSettings.setAppCacheEnabled(true)
        webSettings.userAgentString = webSettings.userAgentString.replace("; wv", "")
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

}
