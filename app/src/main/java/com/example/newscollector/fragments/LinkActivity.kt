package com.example.newscollector.fragments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.example.newscollector.R

class LinkActivity : AppCompatActivity() {

    override fun onBackPressed() = finish()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_link)
        fullscreen()
        intent?.let {
            val b = intent.getStringExtra("url")
            if(b.isNullOrBlank()) showError()
            else loadNews(b)
        }

        val layout : ConstraintLayout = findViewById(R.id.layout_link_base)
        layout.setOnClickListener { finish() }
    }

    private fun loadNews(url : String) {
        val mWebView : WebView = findViewById(R.id.webView_news)
        mWebView.webViewClient = WebViewClient()

        val webSettings = mWebView.settings
        mWebView.scrollBarStyle = View.SCROLLBARS_OUTSIDE_OVERLAY
        mWebView.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.allowContentAccess = true
        webSettings.javaScriptEnabled = false
        webSettings.loadWithOverviewMode = true
        webSettings.useWideViewPort = true
        webSettings.supportZoom()
        webSettings.userAgentString = webSettings.userAgentString.replace("; wv", "")

        mWebView.loadUrl(url)
    }

    private fun showError() {
        Toast.makeText(this, getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show()
        finish()
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }
}