package com.example.newscollector.fragments

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.newscollector.R
import com.example.newscollector.data.News
import com.example.newscollector.data.NewsItem
import com.example.newscollector.fragments.adapter.NewsAdapter
import com.example.newscollector.util.ServerClient
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable


class NewsFeedFragment(private var rss: String) : Fragment() {
    private lateinit var mView : View
    private val handler: Handler = Handler(Looper.getMainLooper())
    val client = ServerClient.create()

    private val sub = object : Observer<News>{
        lateinit var d :Disposable
        override fun onComplete() {
            d.dispose()
        }

        override fun onSubscribe(d: Disposable) {
            this.d = d
        }

        override fun onNext(t: News) {
            fillAdapter(t.channel.newsList)
        }

        override fun onError(e: Throwable) {
            showError()
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
       mView = inflater.inflate(R.layout.fragment_news_feed, container, false)

        loadNews(rss)

        return mView
    }

    private fun loadNews(url : String) {
        handler.post(object : Runnable {
            override fun run() {
                client.getNews(url)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(sub)
                handler.postDelayed(this, 6000)
            }
        })
    }

    private fun fillAdapter(news : List<NewsItem>) {
        val recycler : RecyclerView = mView.findViewById(R.id.recycler_feed)
        recycler.layoutManager = LinearLayoutManager(context)
        val adapter = NewsAdapter(news)
        adapter.setOnTouchCallback(object : NewsAdapter.Callback {
            override fun onClick(position: Int) {
                showNews(news[position].link)
            }
        })
        recycler.adapter = adapter
    }

    private fun showError() = Toast.makeText(context, getString(R.string.msg_error_server), Toast.LENGTH_SHORT).show()

    private fun showNews(url : String?) {
        url?.let{
            val intent = Intent(context, LinkActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NO_ANIMATION
            intent.putExtra("url", url)
            startActivity(intent)
        }
    }
}