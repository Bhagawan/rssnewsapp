package com.example.newscollector.fragments.mvp

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.example.newscollector.data.SplashResponse
import com.example.newscollector.util.ServerClient
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter
import java.util.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@InjectViewState
class SplashPresenter: MvpPresenter<SplashPresenterViewInterface>() {
    private var requestInProcess = false
    private lateinit var target : Target

    fun requestSplash() {
        showLogo()
        ServerClient.create().getSplash(Locale.getDefault().language)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe( object :
            Observer<SplashResponse>{
            lateinit var d :Disposable
            override fun onSubscribe(d: Disposable) {
                this.d = d
            }

            override fun onNext(t: SplashResponse) {
                val s: String = t.url
                if (s.isBlank() || s == "no" ) viewState.switchToMain()
                else {
                    if (requestInProcess) {
                        requestInProcess = false
                        viewState.hideLogo()
                    }
                    viewState.hideLogo()
                    viewState.showSplash("https://$s")
                }
            }

            override fun onError(e: Throwable) {
                viewState.showError("Server error 3")
                viewState.switchToMain()
            }

            override fun onComplete() {
                d.dispose()
            }
        })
    }

    private fun showLogo() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: LoadedFrom?) {
                if (requestInProcess) viewState.showLogo(bitmap)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        requestInProcess = true
        Picasso.get().load("http://195.201.125.8/RssNewsApp/logo.png").into(target)
    }


}