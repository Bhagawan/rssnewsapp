package com.example.newscollector.fragments.adapter

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.newscollector.fragments.NewsFeedFragment

class MyPagerAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 5

    override fun createFragment(position: Int): Fragment = when(position) {
        0 -> NewsFeedFragment("https://www.sport-express.ru/services/materials/news/football/se/")
        1 -> NewsFeedFragment("https://www.sport-express.ru/services/materials/news/hockey/se/")
        2 -> NewsFeedFragment("https://www.sport-express.ru/services/materials/news/basketball/se/")
        3 -> NewsFeedFragment("https://www.sport-express.ru/services/materials/news/tennis/se/")
        4 -> NewsFeedFragment("https://www.sport-express.ru/services/materials/news/formula1/se/")
        else -> { NewsFeedFragment("https://www.yardbarker.com/rss/rumors") }
    }
}