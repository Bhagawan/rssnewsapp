package com.example.newscollector.fragments.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.newscollector.R
import com.example.newscollector.data.NewsItem
import java.text.SimpleDateFormat
import java.util.*

class NewsAdapter (private val news : List<NewsItem>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    lateinit var callback: Callback

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.header.text = news[position].title
        news[position].date?.let {
            val rusLocale  = Locale.Builder().setLanguage("ru").setScript("Cyrl").build()
            val edf = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ZZ", Locale.ENGLISH)
            val rdf = SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", rusLocale)
            val d = edf.parse(it)
            d?.let {
                holder.data.text = rdf.format(d)
            }
        }
        holder.itemView.setOnClickListener { callback.onClick(position) }
    }

    override fun getItemCount(): Int {
        return news.size
    }

    fun setOnTouchCallback(callback :Callback) {
        this.callback = callback
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val header: TextView = itemView.findViewById(R.id.text_item_header)
        val data : TextView = itemView.findViewById(R.id.text_item_data)
    }

    interface Callback {
        fun onClick(position :Int)
    }
}