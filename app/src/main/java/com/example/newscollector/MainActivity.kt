package com.example.newscollector

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.viewpager2.widget.ViewPager2
import com.example.newscollector.fragments.adapter.MyPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var target : Target

    override fun onBackPressed() = finish()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        fullscreen()
        setBackground()

        val tabLayout : TabLayout = findViewById(R.id.tabLayout)
        val viewPager : ViewPager2 = findViewById(R.id.viewPager)
        viewPager.adapter = MyPagerAdapter(this)

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = when(position) {
                0 -> getString(R.string.football)
                1 -> getString(R.string.hockey)
                2 -> getString(R.string.basketball)
                3 -> getString(R.string.tennis)
                4 -> getString(R.string.formula_one)
                else -> ""
            }
        }.attach()
    }

    override fun onResume() {
        super.onResume()
        fullscreen()
    }

    private fun fullscreen() {
        WindowInsetsControllerCompat(window, window.decorView).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
        WindowCompat.setDecorFitsSystemWindows(window, false)
    }

    private fun setBackground() {
        val backLayout = findViewById<ConstraintLayout>(R.id.layout_main)
        val header : TextView = findViewById(R.id.text_main_header)
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                backLayout.background = BitmapDrawable(resources, bitmap)
                header.setTextColor(Color.WHITE)
            }
            override fun onBitmapFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/RssNewsApp/back.png").into(target)
    }
}