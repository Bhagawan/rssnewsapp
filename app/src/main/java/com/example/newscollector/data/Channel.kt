package com.example.newscollector.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml
@Keep
data class Channel(@PropertyElement(name = "title") val title : String?,
                    @Element(name = "item") val newsList : List<NewsItem>)
