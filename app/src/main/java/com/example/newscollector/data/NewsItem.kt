package com.example.newscollector.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Xml
@Keep
data class NewsItem(@PropertyElement(name = "title") val title : String?,
                    @PropertyElement(name = "pubDate") val date : String?,
                    @PropertyElement(name = "description") val description : String?,
                    @PropertyElement(name = "link") val link : String?)