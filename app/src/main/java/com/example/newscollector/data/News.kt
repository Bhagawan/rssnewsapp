package com.example.newscollector.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Xml

@Xml
@Keep
data class News(@Attribute(name = "version") val version: String?,
                @Element(name = "channel") val channel : Channel)

