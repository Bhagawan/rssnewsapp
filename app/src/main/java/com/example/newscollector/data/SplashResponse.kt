package com.example.newscollector.data

import androidx.annotation.Keep

@Keep
data class SplashResponse(val url : String)

